---
title: "The effects of a long-acting monoclonal antibody against respiratory syncytial virus on hospital admission numbers at the University Children's Hospital Bern"
format:
  html: default
  docx: default
execute: 
  echo: false
  warning: false
---

```{r data-prep}
library(here)
library(data.table)
library(MASS)
library(pscl)
library(broom)
library(ggplot2)

# load data ####
load(here("data", "rsv_inselspital_sim.RData"))
rsv <- targets::tar_read(rsv, store = here("_targets"))

# prepare historical data ####
rsv.old <- rsv.hist[, .N, by = .(adm.isoy, adm.isow, season.nr, season, age.grp)]
setnames(
  rsv.old,
  old = c("adm.isoy", "adm.isow", "N"),
  new = c("year", "week", "rsv"),
  skip_absent = TRUE
)
# make age a factor
rsv.old$age.grp <- factor(
  rsv.old$age.grp,
  levels = c("infant", "toddler", "child"),
  labels = c("rsv_less_1y", "rsv_1to2y", "rsv_over_2y")
)
# make weeks a factor
rsv.old$week <- factor(rsv.old$week, levels = c(27:52, 1:26))

# prepare current data ####
rsv.new <- rsv[
  record_id == "Bern" & date > "2024-06-30",
  .(year, week, date, rsv_less_1y, rsv_1to2y, rsv_over_2y)
] |> 
  melt.data.table(
    id.vars = c("year", "week", "date"),
    variable.name = "age.grp",
    value.name = "rsv"
  ) |> 
  _[order(date), ] |> 
  # add season number and season
  _[, season.nr := 28] |> 
  _[, season := "nirsevimab"] |> 
  _[, .(year, week, season.nr, season, age.grp, rsv)] |> 
  _[rsv != 0, ]
# make weeks a factor
rsv.new$week <- factor(rsv.new$week, levels = c(27:52, 1:26))

# combine datasets ####
rsv.comp <- rbindlist(list(rsv.old, rsv.new))
# make season.nr a factor
rsv.comp$season <- factor(rsv.comp$season, levels = c("nirsevimab", "min", "maj"))
```

<!-- ## Relationship between week of admission and admission numbers (positive counts only) -->
<!-- ```{r plot-counts per week} -->
<!-- ggplot(rsv.comp, aes(week, rsv)) + -->
<!--   geom_point(size = 2) + -->
<!--   facet_wrap(facets = vars(season, age.grp), ncol = 3) -->
<!-- ``` -->

<!-- ## Poisson GLM for infants (positive counts only) -->
<!-- $Counts_i \sim Poisson(\mu_i)$   -->
<!-- $E(Counts_i) = \mu_i$   -->
<!-- $var(Counts_i) = \mu_i$   -->
<!-- $log(\mu_i) = \alpha + \beta_1*season_i + \beta_2*week_i$ -->

<!-- ```{r glm-infants} -->
<!-- mod.glm.infant <- glm( -->
<!--   rsv ~ season + week, -->
<!--   family = quasipoisson(link = "log"), -->
<!--   data = rsv.comp[age.grp == "rsv_less_1y"] -->
<!-- ) -->
<!-- # summary(mod.glm.infant) -->
<!-- ``` -->

<!-- ### Over- / underdispersion in the model? -->
<!-- ```{r glm-dispersion} -->
<!-- E2 <- resid(mod.glm.infant, tpye = "pearson") -->
<!-- N <- nrow(rsv.comp[age.grp == "rsv_less_1y"]) -->
<!-- p <- length(coef(mod.glm.infant)) -->
<!-- sum(E2^2) / (N - p) -->
<!-- ``` -->

<!-- ## Negative binomial GLM for infants (positive counts only) -->
<!-- $Counts_i \sim NB(\mu_i, \theta)$   -->
<!-- $E(Counts_i) = \mu_i$   -->
<!-- $var(Counts_i) = (\mu_i + \mu_i^2)/\theta$   -->
<!-- $log(\mu_i) = \alpha + \beta_1*season_i + \beta_2*week_i$ -->

```{r glm-nb-infants}
mod.nb.infant <- glm.nb(
  rsv ~ season + week,
  data = rsv.comp[age.grp == "rsv_less_1y"]
)
```

<!-- ### Over- / underdispersion in the model? -->
<!-- ```{r glm-nb-dispersion} -->
<!-- E2 <- resid(mod.nb.infant, tpye = "pearson") -->
<!-- N <- nrow(rsv.comp[age.grp == "rsv_less_1y"]) -->
<!-- p <- length(coef(mod.nb.infant)) -->
<!-- sum(E2^2) / (N - p) -->
<!-- ``` -->

<!-- ## The same using data including 0 counts -->
<!-- ```{r data-prep-zero} -->
<!-- # prepare historical data #### -->
<!-- rsv.old.zero <- rsv.hist[, .N, by = .(adm.isow, season.nr, season, age.grp)] -->
<!-- setnames( -->
<!--   rsv.old.zero, -->
<!--   old = c("adm.isow", "N"), -->
<!--   new = c("week", "rsv"), -->
<!--   skip_absent = TRUE -->
<!-- ) -->
<!-- # add zero counts -->
<!-- rsv.old.zero <- rsv.old.zero[ -->
<!--   CJ(week = 1:52, age.grp = c("infant", "toddler", "child"), season.nr = 12:22), -->
<!--   on = c("week", "season.nr", "age.grp") -->
<!-- ] -->
<!-- # add missing season classifiers -->
<!-- rsv.old.zero[is.na(season), season := ifelse(season.nr %% 2 == 0, "maj", "min")] -->
<!-- # add zero values -->
<!-- rsv.old.zero[is.na(rsv), rsv := 0] -->
<!-- # make age a factor -->
<!-- rsv.old.zero$age.grp <- factor( -->
<!--   rsv.old.zero$age.grp, -->
<!--   levels = c("infant", "toddler", "child"), -->
<!--   labels = c("rsv_less_1y", "rsv_1to2y", "rsv_over_2y") -->
<!-- ) -->
<!-- # make week a factor -->
<!-- rsv.old.zero$week <- factor(rsv.old.zero$week, levels = c(27:52, 1:26)) -->

<!-- # prepare current data #### -->
<!-- rsv.new.zero <- rsv[ -->
<!--   record_id == "Bern" & date > "2024-06-30", -->
<!--   .(year, week, date, rsv_less_1y, rsv_1to2y, rsv_over_2y) -->
<!-- ] |>  -->
<!--   melt.data.table( -->
<!--     id.vars = c("year", "week", "date"), -->
<!--     variable.name = "age.grp", -->
<!--     value.name = "rsv" -->
<!--   ) |>  -->
<!--   _[order(date), ] |>  -->
<!--   # add season number and season -->
<!--   _[, season.nr := 28] |>  -->
<!--   _[, season := "nirsevimab"] |>  -->
<!--   _[, .(week, season.nr, season, age.grp, rsv)] -->
<!-- # make weeks a factor -->
<!-- rsv.new.zero$week <- factor(rsv.new.zero$week, levels = c(27:52, 1:26)) -->

<!-- # combine datasets #### -->
<!-- rsv.comp.zero <- rbindlist(list(rsv.old.zero, rsv.new.zero)) -->
<!-- # make season.nr a factor -->
<!-- rsv.comp.zero$season <- factor(rsv.comp.zero$season, levels = c("nirsevimab", "min", "maj")) -->
<!-- # add summer, this will be the most likely cause for 0 values, besides the shift between minor and major season -->
<!-- rsv.comp.zero[, summer := ifelse(week %in% 20:38, "yes", "no")] -->
<!-- rsv.comp.zero$summer <- factor(rsv.comp.zero$summer) -->
<!-- ``` -->

<!-- ## Relationship between week of admission and admission numbers -->
<!-- ```{r plot-counts-zero per week} -->
<!-- ggplot(rsv.comp.zero, aes(week, rsv)) + -->
<!--   geom_point(size = 2) + -->
<!--   scale_x_discrete("", breaks = seq(2, 50, 4), labels = c(seq(28, 52, 4), seq(4, 24, 4))) + -->
<!--   facet_wrap(facets = vars(season, age.grp), ncol = 3) -->
<!-- ``` -->

<!-- ## Poisson GLM for infants (including zero counts) -->
<!-- $Counts_i \sim Poisson(\mu_i)$   -->
<!-- $E(Counts_i) = \mu_i$   -->
<!-- $var(Counts_i) = \mu_i$   -->
<!-- $log(\mu_i) = \alpha + \beta_1*season_i + \beta_2*week_i$ -->

<!-- ```{r glm-infants-zero} -->
<!-- mod.glm.infant.zero <- glm( -->
<!--   rsv ~ season + week, -->
<!--   family = quasipoisson(link = "log"), -->
<!--   data = rsv.comp.zero[age.grp == "rsv_less_1y"] -->
<!-- ) -->
<!-- # summary(mod.glm.infant.zero) -->
<!-- ``` -->

<!-- ### Over- / underdispersion in the model? -->
<!-- ```{r glm-dispersion-zero} -->
<!-- E2 <- resid(mod.glm.infant.zero, tpye = "pearson") -->
<!-- N <- nrow(rsv.comp.zero[age.grp == "rsv_less_1y"]) -->
<!-- p <- length(coef(mod.glm.infant.zero)) -->
<!-- sum(E2^2) / (N - p) -->
<!-- ``` -->

<!-- ## Negative binomial GLM for infants (including zero counts) -->
<!-- $Counts_i \sim NB(\mu_i, \theta)$   -->
<!-- $E(Counts_i) = \mu_i$   -->
<!-- $var(Counts_i) = (\mu_i + \mu_i^2)/\theta$   -->
<!-- $log(\mu_i) = \alpha + \beta_1*season_i + \beta_2*week_i$ -->

<!-- ```{r glm-nb-infants-zero} -->
<!-- mod.nb.infant.zero <- glm.nb( -->
<!--   rsv ~ season + week,  -->
<!--   data = rsv.comp.zero[age.grp == "rsv_less_1y"] -->
<!-- ) -->
<!-- # summary(mod.nb.infant.zero) -->
<!-- ``` -->

<!-- ### Over- / underdispersion in the model? -->
<!-- ```{r glm-nb-dispersion-zero} -->
<!-- E2 <- resid(mod.nb.infant.zero, tpye = "pearson") -->
<!-- N <- nrow(rsv.comp.zero[age.grp == "rsv_less_1y"]) -->
<!-- p <- length(coef(mod.nb.infant.zero)) -->
<!-- sum(E2^2) / (N - p) -->
<!-- ``` -->

<!-- ## Zero-inflated Poisson GLM (including zero counts) -->
<!-- $Counts_i \sim ZIP(\mu_i, \pi_i)$   -->
<!-- $E(Counts_i) = \mu_i * (1 - \pi_i)$   -->
<!-- $var(Counts_i) = (1 - \pi_i) * (\mu_i + \pi_i + \mu_i^2)$   -->
<!-- $log(\mu_i) = \beta_1 + \beta_2*season_i + \beta_3*week_i$ -->
<!-- $log(\pi_i) = \gamma_1 + \gamma_2 * season_i + \gamma_3 * week_i$ -->

<!-- ```{r glm-zip-infants-zero} -->
<!-- mod.zip.infant.zero <- zeroinfl( -->
<!--   rsv ~ season + week | season + summer, -->
<!--   dist = "poisson", -->
<!--   data = rsv.comp.zero[age.grp == "rsv_less_1y"] -->
<!-- ) -->
<!-- # summary(mod.zip.infant.zero) -->
<!-- ``` -->

<!-- ### Over- / underdispersion in the model? -->
<!-- ```{r glm-zip-dispersion-zero} -->
<!-- E2 <- resid(mod.zip.infant.zero, tpye = "pearson") -->
<!-- N <- nrow(rsv.comp.zero[age.grp == "rsv_less_1y"]) -->
<!-- p <- length(coef(mod.zip.infant.zero)) -->
<!-- sum(E2^2) / (N - p) -->
<!-- ``` -->

<!-- ## Zero-inflated negative binomial GLM (including zero counts) -->

<!-- ```{r glm-zipnb-infants-zero} -->
<!-- mod.zipnb.infant.zero <- zeroinfl( -->
<!--   rsv ~ season + week | season + summer, -->
<!--   dist = "negbin", -->
<!--   data = rsv.comp.zero[age.grp == "rsv_less_1y"] -->
<!-- ) -->
<!-- # summary(mod.zipnb.infant.zero) -->
<!-- ``` -->

<!-- ### Over- / underdispersion in the model? -->
<!-- ```{r glm-zipnb-dispersion-zero} -->
<!-- E2 <- resid(mod.zipnb.infant.zero, tpye = "pearson") -->
<!-- N <- nrow(rsv.comp.zero[age.grp == "rsv_less_1y"]) -->
<!-- p <- length(coef(mod.zipnb.infant.zero)) -->
<!-- sum(E2^2) / (N - p) -->
<!-- ``` -->

<!-- ## Comparison of methodology -->
<!-- Looks like Negative Binomial regression using positive counts only is doing best based on a rough check of overdispersion. It is certainly better than Poisson regression with positive count only. When introducing zero counts, the zero-inflated Negative Binomial regression is again better, however less so than Negative Binomial regression. Zero-inflated models did only work once I introduced a seasonal variable (week 20 until week 38), otherwise there was an issue with complete separation. -->
<!-- For the SGP meeting I suggest to use Negative Binomial regression.  -->

{{< pagebreak >}}

```{r models-used}
# we'll use the neg. binomial models. First tidy them up
nb.mod.nirse.inf <- tidy(mod.nb.infant, conf.int = TRUE, exponentiate = TRUE)
nb.mod.nirse.tod <- tidy(
  glm.nb(rsv ~ season + week, data = rsv.comp[age.grp == "rsv_1to2y"]),
  conf.int = TRUE, exponentiate = TRUE
)
nb.mod.nirse.child <- tidy(
  glm.nb(rsv ~ season + week, data = rsv.comp[age.grp == "rsv_over_2y"]),
  conf.int = TRUE, exponentiate = TRUE
)

show_perc_change <- function(model, season) {
  pos <- ifelse(season == "maj", 3L, 2L)
  est <- 1 - 1 / model$estimate[pos]
  lci <- 1 - 1 / model$conf.low[pos]
  uci <- 1 - 1 / model$conf.high[pos]
  print.est <- ifelse(est < 0, paste0("+", abs(round(est * 100))), paste0("-", abs(round(est * 100))))
  print.lci <- ifelse(lci < 0, paste0("+", abs(round(lci * 100))), paste0("-", abs(round(lci * 100))))
  print.uci <- ifelse(uci < 0, paste0("+", abs(round(uci * 100))), paste0("-", abs(round(uci * 100))))
  return(paste0(print.est, "%", " [95%CI ", print.uci, " -- ", print.lci, "%]"))
}

```


## Background
Historically, we have observed a biannual alternation of major and minor Respiratory Syncytial Virus (RSV) seasons at the University Children’s Hospital Bern, based on hospital admission numbers. This cycle was disrupted by non-pharmaceutical interventions during the COVID-19 pandemic. After two unusually large winter seasons in succession, winter 2024/2025 is the first season with general availability of a highly effective, long-acting monoclonal antibody against RSV in infants in Switzerland. In this analysis, we compared hospital admission numbers during the 2024/2025 winter season to historical data prior to the COVID-19 pandemic.

## Methods
Retrospective, single-center study of all children < 5 years, admitted to the Inselspital Bern with a RSV infection during 12 winter seasons. We compared weekly admission numbers between 2008 and 2019 to the 2024/2025 winter season. RSV hospitalization was defined as hospital admission with virologically confirmed RSV infection. We used a Negative Binomial regression model to analyse weekly admission numbers with season strength and week of admission as independent variables.

## Results
Prior to the COVID-19 pandemic on average `r quantile(rsv.hist[season == "min" & age.grp == "infant", .N, by = season.nr][, N], probs = 0.5)` (IQR `r quantile(rsv.hist[season == "min" & age.grp == "infant", .N, by = season.nr][, N], probs = 0.25)`-`r quantile(rsv.hist[season == "min" & age.grp == "infant", .N, by = season.nr][, N], probs = 0.75)`) infants were hospitalised in minor and `r round(quantile(rsv.hist[season == "maj" & age.grp == "infant", .N, by = season.nr][, N], probs = 0.5))` (IQR `r round(quantile(rsv.hist[season == "maj" & age.grp == "infant", .N, by = season.nr][, N], probs = 0.25))`-`r round(quantile(rsv.hist[season == "maj" & age.grp == "infant", .N, by = season.nr][, N], probs = 0.75))`) in major seasons. As of week `r rsv[year == 2025 & record_id == "Bern", max(week)]` in 2025, `r rsv.new[age.grp == "rsv_less_1y", sum(rsv)]` infants have been hospitalised in the 2024/2025 winter season. Compared to historical minor seasons, we observed an average reduction of weekly admission numbers by `r round((1 - 1 / nb.mod.nirse.inf$estimate[2]) * 100)`% (95%CI `r round((1 - 1 / nb.mod.nirse.inf$conf.low[2]) * 100)` -- `r round((1 - 1 / nb.mod.nirse.inf$conf.high[2]) * 100)`%, `r writeutil::script_pval(nb.mod.nirse.inf$p.value[2], signif = 4)`) and by `r round((1 - 1 / nb.mod.nirse.inf$estimate[3]) * 100)`% (95%CI `r round((1 - 1 / nb.mod.nirse.inf$conf.low[3]) * 100)` -- `r round((1 - 1 / nb.mod.nirse.inf$conf.high[3]) * 100)`%, `r writeutil::script_pval(nb.mod.nirse.inf$p.value[3])`) compared to major seasons. In contrast, we did not see relevant changes in average weekly admission numbers in children between 1 and 2 years compared to historical minor (`r show_perc_change(nb.mod.nirse.tod, "min")`) or major (`r show_perc_change(nb.mod.nirse.tod, "maj")`) seasons. The same was true for hospital admissions in older children (`r show_perc_change(nb.mod.nirse.child, "min")` and `r show_perc_change(nb.mod.nirse.child, "maj")`, respectively).

## Conclusion
In the first winter season with general availability of a long-acting monoclonal antibody against RSV, we have so far seen a reduction in the number of infants admitted with RSV infection. Based on comparison with historical data, this reduction is likely due to the effect of general RSV prophylaxis and not only explained by alternating season strength.

Numbers will be updated for the final presentation.
