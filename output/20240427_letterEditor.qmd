---
title: "Letter to Editor"
format: html
editor: visual
---

Dear editor,

In this article we present data on the current epidemiology of respiratory syncytial virus infections in children in Switzerland collected by a clinician network. This expands on the data we have previously published in Swiss Medical Weekly (PMID: 34499459). We show ongoing alterations of the epidemiology of RSV in children compared to the pre-Covid-19 area.

We think this data is valuable to the readers of Swiss Medical Weekly, as it gives a current and timely update on the number of cases of RSV infections seen in Swiss children hospitals. Such data are valuable, given the imminent introduction of long-acting antibodies for the prevention of RSV in children in Switzerland and the soon to be expected introduction of a maternal vaccine.

The manuscript has not been published before and is not currently under consideration by another journal.
