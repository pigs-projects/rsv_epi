---
title: "RSV EpiCH - 2024"
format:
  revealjs:
    theme: [default, custom.scss]
editor: source
---

```{r setup}
library(here)
library(data.table)
library(ggplot2)

targets::tar_load(rsv, store = here("_targets"))
survey <- fread(here("data", "20240428_RSVEPI_surveymonkey.csv"))
setDT(survey)

# default colours for graphs
bg_col = "#fafafa"
l_col = "#abb0b6"

# default theme for graphs
std_theme <- theme(
    # panel.grid = element_blank(),
    panel.grid = element_blank(),
    panel.background = element_blank(),
    axis.line.y = element_line(linewidth = 0.5),
    axis.line.x = element_line(linewidth = 0.5),
    text = element_text(size = 16),
    plot.background = element_rect(fill = bg_col),
    legend.background = element_rect(fill = bg_col),
    strip.background = element_rect(fill = bg_col, colour = NA),
    legend.position.inside = c(0.2, 0.7)
)

hosp_labels <- c(
  `regional hospital` = "Regional hospitals",
  `University hospital` = "University hospitals"
)
```

```{r prep-data}
# systematically separate comment from response
survey[
  , c("can_provide_hosp_nr", "hosp_nr_comment") := tstrsplit(
    can_provide_hosp_nr, " – ", fixed = TRUE
  )
]
survey[
  , c("can_provide_picu", "picu_comment") := tstrsplit(
    can_provide_picu, " – ", fixed = TRUE
  )
]
survey[
  , c("other_severity_info", "severity_comment") := tstrsplit(
    other_severity_info, " – ", fixed = TRUE
  )
]
survey[
  , c("can_capture_prophylaxis", "prophylaxis_comment") := tstrsplit(
    can_capture_prophylaxis, " – ", fixed = TRUE
  )
]
survey[
  , c("can_provide_weekly_aggregates", "weekly_comment") := tstrsplit(
    can_provide_weekly_aggregates, " – ", fixed = TRUE
  )
]
survey[
  , c("can_provide_data_retrospectively", "retrospectively_comment") := tstrsplit(
    can_provide_data_retrospectively, " – ", fixed = TRUE
  )
]

# convert NA to No 
survey[is.na(other_severity_info), other_severity_info := "No"]

# harmonise probably
survey[
  stringr::str_detect(can_provide_weekly_aggregates, "weekly"),
  can_provide_weekly_aggregates := "Probably"
]

survey[
  stringr::str_detect(
    can_provide_data_retrospectively, "Probably"),
  can_provide_data_retrospectively := "Probably"
]

```

---

```{r plot-season}
errbar_y <- 590
errbar_text_y <- 610
errbar_w <- 10

fig1_data <- melt(
  rsv,
  id.vars = c("date", "record_id"),
  measure.vars = c("rsv_less_1y", "rsv_1to2y", "rsv_over_2y"),
  variable.name = "age.grp",
  value.name = "rsv",
)[, .(tot.rsv = sum(rsv, na.rm = TRUE)), by = .(date, age.grp)]

fig1 <- ggplot(fig1_data, aes(date, tot.rsv, fill = forcats::fct_rev(age.grp))) +
  geom_col() +
  scale_fill_brewer(
    "Age group", type = "qual",
    labels = c("2-16 years", "12 to 23 months", "0-11 months")) +
  scale_y_continuous("Number of RSV detections", breaks = seq(0, 600, 100)) +
  scale_x_date(
    "",
    breaks = as.Date(
      c("2021-01-01", "2021-07-01", "2022-01-01", "2022-07-01", "2023-01-01",
        "2023-07-01", "2024-01-01")
    ),
    date_labels = "%B\n%Y") +
  # add annotations for the 3 seasons we defined
  ## 2021/2022 pandemic period
  annotate(
    geom = "errorbar", xmin = as.Date("2021-01-04"),
    xmax = as.Date("2022-06-30"), y = errbar_y, width = errbar_w
  ) +
  annotate(
    geom = "text", x = as.Date("2021-10-01"), y = errbar_text_y,
    label = "2021/2022 pandemic period", size = 4
  ) +
  ## 2022/2023 winter season
  annotate(
    geom = "errorbar", xmin = as.Date("2022-07-04"),
    xmax = as.Date("2023-06-30"), y = errbar_y, width = errbar_w
  ) +
  annotate(
    geom = "text", x = as.Date("2023-01-01"), y = errbar_text_y,
    label = "2022/2023 winter season", size = 4
  ) +
  ## 2023/2024 winter season
  annotate(
    geom = "errorbar", xmin = as.Date("2023-07-03"),
    xmax = max(rsv$date) + 7, y = errbar_y, width = errbar_w
  ) +
  # hack to have right open line for current season
  annotate(
    geom = "errorbar", xmin = max(rsv$date),
    xmax = max(rsv$date) + 7, y = errbar_y, width = errbar_w + 0.5,
    colour = bg_col,
    linewidth = 0.7
  ) +
  annotate(
    geom = "text", x = as.Date("2023-11-15"), y = errbar_text_y,
    label = "2023/2024 winter season", size = 4
  ) +
  coord_cartesian(xlim = c(as.Date("2020-12-15"), as.Date("2024-03-25"))) +
  std_theme

fig1
```

- National baseline data pre Nirsevimab?\n
- Example <https://nirsegal.es>

## Suggested changes

:::: {.columns}

::: {.column}
- capture *N* in hospital
- capture *N* in ICU
- capture *N* in hospital with prophylaxis
- Interest by EKIF/BAG

:::

::: {.column}

### Survey RSV EpiCH
- `r nrow(survey)` (`r round(nrow(survey) / 18 * 100)`%) of 18 respondet (`r survey[site == "University hospital", .N]` University hospitals)
- Feasibility?

:::

::::

## Site characteristics

:::: {.columns}

::: {.column}
```{r plot-systematic-testing}
#| fig-width: 5
#| fig-height: 6

# prepare annotation data
test_annotation <- dcast(
  survey,
  site ~ testing_strategy,
  value.var = "id",
  fun.aggregate = length
)
test_annotation[, tot := sum(.SD), .SDcols = c(2,3), by = site]
test_annotation[, c("perc1", "perc2") := lapply(.SD, \(x) x / tot), .SDcols = c(2, 3), by = site]

ggplot(survey, aes(testing_strategy)) +
  geom_bar() +
  scale_x_discrete("", labels = c("No", "Yes\nall<2", "Yes\nall <5")) +
  facet_wrap(facets = vars(site), labeller = labeller(
    site = hosp_labels
  )) +
  labs(title = "Systematic testing of hospitalised?",
       x = "", y = "") +
  std_theme
```

:::

::: {.column}

In `r round(survey[testing_strategy != "No", .N] / survey[, .N] * 100)`% systematic testing

:::

::::

## Capture of hospitalised children

:::: {.columns}

::: {.column} 
```{r plot-hospitalised}
#| fig-width: 5
#| fig-height: 6

ggplot(survey, aes(can_provide_hosp_nr)) +
  geom_bar() +
  facet_wrap(facets = vars(site), labeller = labeller(
    site = hosp_labels
  )) +
  labs(
    title = "Can you provide N admitted?",
    x = "",
    y = "",
    caption = "Probably = need support") +
  std_theme
```
:::

::: {.column}

`r round(survey[testing_strategy != "No", .N] / survey[, .N] * 100)`% can capture admission numbers

:::

::::

## Severity

:::: {.columns}

::: {.column}
```{r plot-picu}
#| fig-width: 5
#| fig-height: 6

ggplot(survey, aes(can_provide_picu)) +
  geom_bar() +
  facet_wrap(facets = vars(site), labeller = labeller(
    site = hosp_labels
  )) +
  labs(
    title = "Can you provide N on PICU",
    x = "",
    y = "",
    caption = "Regional hospitals: 2 IMC\nHow: 2 need additional support"
  ) +
  std_theme
```
:::

::: {.column}
```{r plot-add-severity}
#| fig-width: 5
#| fig-height: 6

ggplot(survey, aes(other_severity_info)) +
  geom_bar() +
  facet_wrap(facets = vars(site), labeller = labeller(
    site = hosp_labels
  )) +
  labs(
    title = "Should we capture other severity data",
    x = "",
    y = "",
    caption = "What: 2 respiratory support / 1 LOS / 1 score\nHow: 4 need additional support"
  ) +
  std_theme
```
:::

::::

## Capture additional data

:::: {.columns}

::: {.column}
```{r plot-nirsevimab}
#| fig-width: 5
#| fig-height: 6

ggplot(survey, aes(can_capture_prophylaxis)) +
  geom_bar() +
  facet_wrap(facets = vars(site), labeller = labeller(
    site = hosp_labels
  )) +
  labs(title = "Can capture prophylaxis",
       x = "", y = "") +
  std_theme

```
:::

::: {.column}
`r round(survey[can_capture_prophylaxis != "No", .N] / survey[, .N] * 100)`% should be able to capture prophylaxis. But needs administrative support.
:::

::::

## Technical aspects

:::: {.columns}

::: {.column}
```{r plot-weekly}
#| fig-width: 5
#| fig-height: 6

ggplot(survey, aes(can_provide_weekly_aggregates)) +
  geom_bar() +
  facet_wrap(facets = vars(site), labeller = labeller(
    site = hosp_labels
  )) +
  labs(
    title = "How to capture data",
    x = "",
    y = "",
    caption = "1: larger interval, 2: enter individual data"
  ) +
  std_theme
```
:::

::: {.column}
```{r plot-retrospect}
#| fig-width: 5
#| fig-height: 6

ggplot(survey, aes(can_provide_data_retrospectively)) +
  geom_bar() +
  facet_wrap(facets = vars(site), labeller = labeller(
    site = hosp_labels
  )) + 
  labs(title = "Retrospective capture possible",
       x = "", y = "", caption = "6 would need administrative support") +
  std_theme
```
:::
::::

## Next steps

- Finalise additional items to be collected (hospital admission, PICU admission, Prophylaxis)
- jurisdictional inquiry ethics committee (weekly aggregates should be fine)
- Meeting with BAG regarding funding options
- changes to redcap data entry to include additional information
