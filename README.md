# RSV Epi R code - Version 0.2

## Setup
I work with Rstudio's project setup and use the `library(here)` to identify the folders. I have the following folders in my project directory: **code**, **data**, **figures**, **output**. If you clone the repository, you will only get **code** and **output**, as the others contain files that are not synchronized.

### R Version (in case things do not work)
R version: 4.0.3 (2020-10-10)
Platform: x86_64-apple-darwin17.0 (64-bit),
Running under: macOS Big Sur 10.16

### libraries needed
The following libraries should be installed on your system for the code to work (All can be downloaded from *CRAN*, but ggswissmaps needs at least Version 0.1.2, currently available from github)

`library(here)`  
`library(data.table)`  
`library(dplyr)`  
`library(flexdashboard)`  
`library(ggplot2)`  
`library(geofacet)`  
`library(ggswissmaps)`  
`library(grid)`  
`library(gridtext)`  
`library(patchwork)`  
`library(flextable)`  
`library(magrittr)`  
`library(readr)`  
`library(tidyr)`  
`library(ISOweek)`  
`library(stringr)`  

#### to use the REDCap API service, the following additional libraries are used
`library(REDCapR)`
`library(keyring)`

#### to use the targets workflow we need
`library(targets)`
`library(tarchetypes)`

#### for the compilation of the pdf report we need the following latex packages
`\usepackage{lastpage}`
`\usepackage{color}`
`\usepackage{fancyhdr}`
`\pagestyle{fancy}`
`\usepackage{lscape}`

### coding style
I use the `library(styler)` to style the code. Don't like all but most what it does, and think it will be easier to collaborate if we use an uniform style.

## Change to `targets` workflow
As of 01.07.2021 I have changed the code base to adhere to the targets workflow. This should allow a better overview of the different dependencies of data, graphs, tables, and reports. targets (i.e. functions) are only run, if their code, data used or upstream dependencies have changed.

## How to get started
Run `targets::tar_make()` in the R console to update all plots, the table, the report, and the dashboard. 
You can use `targets::tar_glimpse()` to show an overview of the different functions and how they are connected.  
The generated targets (see the _targets.R file) are available using `targets::tar_load()` or `targets::tar_read()` see help for the difference. Too see how this might be useful you can have a look at the code of the dashboard and report, where both functions are used.

## Using the REDCap API
First you need to request an API token from within REDCap. To avoid hardcoding my token in the script I used the `keyring` library to save the token. Theoretically, this should allow saving the token with `keyring::key_set()` and then retrieve with `keyring::key_get()`. However, I could not save the token from within R. As a workaround, I saved the token in the keychain on my mac and then revtrieved using `keyring::key_get("Item_name_in_Keychain")`.

## Using downloaded data
If you cannot use the REDCap API, you can download the data directly from REDCap and save in the data folder (see setup above). You then have to switch the variable `use_redcap_api` in the _targets.R file to `FALSE`. The function should then use the latest data downloaded from REDCap. Cave the code assumes that only data downloaded from the RSV EpiCH REDCap site is stored in the data folder, it might break if you store different data in that folder. 

## How to modificate plots, and table etc. 
Many of the variables used to define the data selection window and the size of text in the graphs are now available in the _targets.R file. With further iterations I aim to make all useful variables available ther which should allow rapid changes and updates to graphs. 

### Dashboard
The appearance of the dashboard can be modified using the `.css` framework. I am a bit at a loss but have added the **.../projectdir/output/style.css** file that can be used as a starting point. 

### Logos
Currently we combine the *RSV EpiCH* and *PIGS* logo for the dashboard. For the dashboard `logo.png` images needs to conform to certain dimensions (see https://rmarkdown.rstudio.com/flexdashboard/using.html#logo__favicon), specifically a heigth of 48 pixels. I converted our new logo with the `library(magick)` package which worked easy enough. Code example below

`img <- magick::image_read(here("location", "imgname.png"))`  
`magick::image_write(magick::image_scale(img, "x48"), path = here("location", "newimgname.png"), format = "png")`

### Automated report
The report is automatically generated based on the data provided. The text blocks that are used to generate the report are available in the two .csv files *report_textlink_1to4w_all.csv* (for the analysis of the data of all sites combined) and *report_textlink_1to4w_sites.csv* (for the analysis of the data of individual sites). For the analysis of the combined data (sum of all sites) the logic by which the text blocks are selected depends on the 1-week and 4-week trend. For the analysis of the individual site data, the logic relies on the overall 4-week-trend, and the per site 1-week trend (specifically the number of sites with with increasing, stable, or decreasing number of RSV cases). Additionally, the text snippets in *report_textlink_1to4w_sites.csv* rely on inline r-code that is evaluated by `glue::glue()`.
