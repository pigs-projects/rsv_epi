# plot themes ####
define_sum_plot_theme <- function(text_size) {
  weekly_theme <- theme(
    panel.grid = element_blank(),
    panel.background = element_blank(),
    axis.line.y = element_line(linewidth = 0.5),
    axis.line.x = element_blank(),
    text = element_text(size = text_size),
    strip.background = element_rect(fill = "white", colour = NA)
  )
  
  return(weekly_theme)
}

season_theme <- function(text_size) {
  yearly_theme <- theme(
    panel.grid = element_blank(),
    panel.background = element_blank(),
    axis.line.y = element_line(linewidth = 0.5),
    axis.line.x = element_line(linewidth = 0.5),
    text = element_text(size = text_size),
    strip.background = element_rect(fill = "white", colour = NA),
    legend.position.inside = c(.15, .8),  # put legend in left upper corner
    legend.key = element_rect(fill = "white")  # remove grey background from legend keys (coloured lines)
  )
  
  return(yearly_theme)
}

define_map_plot_theme <- function(text_size_legend, text_size_strip) {
  map_theme <- theme(
    panel.grid = element_blank(),
    panel.background = element_rect(fill = "transparent", colour = "NA"),
    plot.background = element_rect(fill = "transparent", colour = "NA"),
    axis.ticks = element_blank(),
    axis.text = element_blank(),
    axis.line = element_blank(),
    legend.text = element_text(size = text_size_legend),
    strip.text = element_text(size = text_size_strip),
    strip.background = element_rect(fill = NA, colour = NA)
  )
  
  return(map_theme)
}

define_col <- function() {
  okabe <- c(orange = "#E69F00", lightblue = "#56B4E9", green = "#009E73",
             yellow = "#F0E442", blue = "#0072B2", red = "#D55E00",
             pink = "#CC79A7", black = "#000000")
  
  return(okabe)
}

# plot heights ####
define_plot_height <- function(data_main, data_map) {#, data_map
  # define min height of plots
  min_h_main <- ifelse(max(data_main$rsv, na.rm = TRUE) < 50, 50,
                       max(data_main$rsv, na.rm = TRUE) + 2
  )
  min_h_rate <- ifelse(max(data_main$det_rate, na.rm = TRUE) < 0.15, 0.15,
                       max(data_main$det_rate, na.rm = TRUE) + 0.05
  )
  min_h_map <- ifelse(max(data_map$rsv, na.rm = TRUE) < 15, 15,
                      max(data_map$rsv, na.rm = TRUE) + 2
  )
  
  height <- list(min_h_main = min_h_main, min_h_rate = min_h_rate,
                 min_h_map = min_h_map)
  
  return(height)
}

# Overall weekly numbers ####
# weekly plots ####
plot_weekly_n <- function(data, lang, plot_theme, plot_height, plot_start_d) {
  
  # define plot dates, height, notations in correct language
  label <- dplyr::case_when(
    lang == "de" ~ "Anzahl RSV Infektionen",
    lang == "fr" ~ "Nombre d'infections VRS",
    lang == "eng" ~ "Number of RSV infections"
  )
  
  # plot
  plot <- ggplot(
    data,
    aes(x = date, y = rsv)
  ) +
    geom_col() +
    ylab(label) +
    scale_x_date(
      name = "", date_breaks = "1 months",
      date_labels = "%b"
    ) +
    coord_cartesian(
      xlim = c(plot_start_d, Sys.Date()),
      ylim = c(0, plot_height)
    ) +
    plot_theme
  
  return(plot)
}

plot_weekly_rate <- function(data, lang, plot_theme, plot_height, plot_start_d) {
  
  # define notations in correct language
  label <- dplyr::case_when(
    lang == "de" ~ "Nachweisrate",
    lang == "fr" ~ "Taux de positivité",
    lang == "eng" ~ "Detection rate"
  )
  
  # plot
  plot <- ggplot(
    data,
    aes(x = date, y = det_rate)
  ) +
    geom_line() +
    scale_y_continuous(name = label, label = scales::percent) +
    scale_x_date(
      name = "", date_breaks = "1 months",
      date_labels = "%b"
    ) +
    scale_colour_discrete("") +
    coord_cartesian(
      xlim = c(plot_start_d, Sys.Date()),
      ylim = c(0, plot_height)
    ) +
    plot_theme
  
  return(plot)
}

plot_combined_weekly <- function(sum_plot, rate_plot, lang) {
  # set language for plotting dates
  locale <- dplyr::case_when(
    lang == "de" ~ "de_CH.UTF-8",
    lang == "fr" ~ "fr_CH.UTF-8",
    lang == "eng" ~ ""
  )
  
  # set language for filenames
  file_lang <- dplyr::case_when(
    lang == "de" ~ "de",
    lang == "fr" ~ "fr",
    lang == "eng" ~ "eng"
  )
  
  # construct filename
  filename <- here("figures", paste0(
    gsub("-", "", Sys.Date()), "_detection_all_", file_lang, ".png"))
  
  # save plot as .png file
  Sys.setlocale(category = "LC_TIME", locale = locale)
  
  plot <- sum_plot + rate_plot +
    plot_layout(nrow = 2, byrow = FALSE, heights = c(3, 1))  # + plot_annotation(title = "Wöchentliche Zahl der RSV-Infektionen")
  
  ragg::agg_png(filename = filename, width = 3508, height = 2480, res = 300)
  print(plot)
  dev.off()
  
  # reset locale back to standard
  Sys.setlocale(category = "LC_TIME", locale = "")
  
  return(filename)
}

# geofaceting plot ####

## geofaceting plot by region ####
plot_geofacets <- function(data, lang, plot_theme, plot_height, plot_start_d,
                           rsv.col) {
  
  # define plot dates, height, notations in correct language
  label_low <- dplyr::case_when(
    lang == "de" ~ "<5 Fälle pro Spital",
    lang == "fr" ~ "<5 cas par hôpital",
    lang == "eng" ~ "<5 cases per hospital"
  )
  label_mid <- dplyr::case_when(
    lang == "de" ~ "5-10 Fälle pro Spital",
    lang == "fr" ~ "5-10 cas par hôpital",
    lang == "eng" ~ "5-10 cases per hospital"
  )
  label_high <- dplyr::case_when(
    lang == "de" ~ ">10 Fälle pro Spital",
    lang == "fr" ~ ">10 cas par hôpital",
    lang == "eng" ~ ">10 cases per hospital"
  )
  
  # define grid for geofacets (we will need to do something different for sentinella)
  epirsv.grid <- data.frame(
    code = c("AG", "BS", "TI", "BE", "GR", "FR", "GE", "VD", "LU", "NE", "TG",
             "SG", "VS", "WT", "ZH"),
    name = c("AG", "BS", "TI", "BE", "GR", "FR", "GE", "VD", "LU", "NE", "KSM",
             "SG", "VS", "KSW", "ZH"),
    row = c(2, 1, 5, 3, 3, 3, 5, 4, 3, 2, 1, 2, 6, 1, 2),
    col = c(4, 3, 5, 3, 6, 2, 1, 2, 4, 2, 6, 6, 3, 5, 5),
    stringsAsFactors = FALSE
  )
  
  # construct plot
  rsv_geofacet <- ggplot(  
    data
  ) +
    geom_col(mapping = aes(x = date, y = rsv, fill = colour)) +
    geom_line(mapping = aes(x = date, y = roll_avg)) +
    scale_y_continuous("", labels = NULL) +
    scale_x_date(name = "", date_breaks = "1 months", labels = NULL) +
    scale_fill_manual("", values = c("low" = unname(rsv.col["blue"]),
                                     "mid" = unname(rsv.col["green"]),
                                     "high" = unname(rsv.col["red"])),
                      breaks = c("low", "mid", "high"),
                      labels = c(label_low, label_mid, label_high)) +
    coord_cartesian(
      xlim = c(plot_start_d, Sys.Date()),
      ylim = c(0, plot_height)
    ) +
    plot_theme +
    facet_geo(~geofacet, grid = epirsv.grid, label = "name")
  
  return(rsv_geofacet)
}

# geofacetting plot by region and age ####

plot_geofacets_age <- function(data, lang, plot_theme, plot_height, plot_start_d) {
  
  # define plot dates, height, notations in correct language
  label_low <- dplyr::case_when(
    lang == "de" ~ "Alter < 1 Jahr",
    lang == "fr" ~ "Âge moins d'un ans",
    lang == "eng" ~ "less than 1 year old"
  )
  label_mid <- dplyr::case_when(
    lang == "de" ~ "Alter 1-2 Jahre",
    lang == "fr" ~ "Àge 1 à 2 ans",
    lang == "eng" ~ "1 to 2 years old"
  )
  label_high <- dplyr::case_when(
    lang == "de" ~ "Älter als 2 Jahre",
    lang == "fr" ~ "plus que 2 ans",
    lang == "eng" ~ "older than 2 years"
  )
  
  # define grid for geofacets (we will need to do something different for sentinella)
  epirsv.grid <- data.frame(
    code = c("AG", "BS", "TI", "BE", "GR", "FR", "GE", "VD", "LU", "NE", "TG",
             "SG", "VS", "WT", "ZH"),
    name = c("AG", "BS", "TI", "BE", "GR", "FR", "GE", "VD", "LU", "NE", "KSM",
             "SG", "VS", "KSW", "ZH"),
    row = c(2, 1, 5, 3, 3, 3, 5, 4, 3, 2, 1, 2, 6, 1, 2),
    col = c(4, 3, 5, 3, 6, 2, 1, 2, 4, 2, 6, 6, 3, 5, 5),
    stringsAsFactors = FALSE
  )
  
  # construct plot
  rsv_geofacet <- ggplot(  
    data
  ) +
    geom_line(mapping = aes(x = date, y = rsv, colour = forcats::fct_rev(age)),
              linewidth = 0.8) +
    scale_y_continuous("", labels = NULL) +
    scale_x_date(name = "", date_breaks = "1 months", labels = NULL) +
    scale_colour_manual("", values = c("rsv_less_1y" = "#7570b3",
                                     "rsv_1to2y" = "#1b9e77",
                                     "rsv_over_2y" = "#d95f02"),
                      breaks = c("rsv_less_1y", "rsv_1to2y", "rsv_over_2y"),
                      labels = c(label_low, label_mid, label_high)) +
    coord_cartesian(
      xlim = c(plot_start_d, Sys.Date()),
      ylim = c(0, plot_height)
    ) +
    plot_theme +
    facet_geo(~geofacet, grid = epirsv.grid, label = "name")
  
  return(rsv_geofacet)
}

## Swiss map as a background ####
plot_chmap <- function() {
  
  data("shp_df", package = "ggswissmaps")
  
  chmap <- ggplot(data = shp_df[["g1l15"]],
                  aes(x = long, y = lat, group = group)) +
    geom_path(colour = "grey65") +
    coord_equal() +
    theme(
      panel.grid = element_blank(),
      panel.background = element_blank(),
      axis.ticks = element_blank(),
      axis.title = element_blank(),
      axis.text = element_blank(),
      axis.line = element_blank(),
      text = element_text(size = 18),
      strip.background = element_rect(fill = "white", colour = NA)
    )
  
  return(chmap)
}

## combine geofacet plot and map plot to final product ####
plot_combined_maps <- function(facetplot, chmap, lang, map_start_d, end_date) {
  
  # pigs logo
  logo <- magick::image_read(here("output", "logo_pigs.png"))
  
  # define caption in correct language
  caption <- dplyr::case_when(
    lang == "de" ~ "Die schwarze Linie zeigt den 3 Wochen Mittelwert<br>AG = Kinderspitäler Aarau und Baden<br>BE = Kinderspitäler Bern und Biel<br>TI = Kinderspitäler Bellinzona, Locarno, Lugano und Mendrisio<br>VS = Spital Wallis und Spital Riviera-Chablais<br>ZH = Kinderspitäler Kispi ZH und Triemli",
    lang == "fr" ~ "La ligne noir indique la valeur moyenne sur 3 semaines<br>AG = Hôpitaux d\\'Aarau et de Baden<br>BE = Hôpitaux de Berne et de Bienne<br>TI = Hôpitaux de Bellinzone, Locarno, Lugano et Mendrisio<br>VS = Hôpital du Valais et Hôpital Riviera-Chablais<br>ZH = Hôpital des enfants \\'Kispi\\' et Hôpital de Triemli",
    lang == "eng" ~ "The black line indicates the 3-week average<br>AG = Hospitals Aarau and Baden<br>BE = Hospitals Bern and Biel<br>TI = Hospitals Bellinzona, Locarno, Lugano and Mendrisio<br>VS = Valais Hospital and Hospital Riviera-Chablais<br>ZH = Hospitals Kispi ZH and Triemli"
  )
  
  date_stamp_text <- dplyr::case_when(
    lang == "de" ~ glue::glue(
      "RSV EpiCH (P. Agyeman, J. Trück, P. Meyer Sauteur)<br>Daten von {format(map_start_d, '%d.%m.%Y')} bis {format(end_date + 6, '%d.%m.%Y')}<br>Stand {format(Sys.Date(), '%d.%m.%Y')}"
    ),
    lang == "fr" ~ glue::glue(
      "RSV EpiCH (P. Agyeman, J. Trück, P. Meyer Sauteur)<br>Données de {format(map_start_d, '%d.%m.%Y')} à {format(end_date + 6, '%d.%m.%Y')}<br>État {format(Sys.Date(), '%d.%m.%Y')}"
    ),
    lang == "eng" ~ glue::glue(
      "RSV EpiCH (P. Agyeman, J. Trück, P. Meyer Sauteur)<br>Data from {format(map_start_d, '%d.%m.%Y')} to {format(end_date + 6, '%d.%m.%Y')}<br>Status {format(Sys.Date(), '%d.%m.%Y')}"
    )
  )
  
  annotation_font <- grid::gpar(fontsize = 11)
  
  # generate text grob for date stamp
  date_stamp_grob <- gridtext::richtext_grob(
    date_stamp_text,
    x = grid::unit(0, "npc"),
    hjust = 0,
    gp = annotation_font
  )
  
  # generate caption grob
  caption_grob <- gridtext::richtext_grob(
    caption,
    x = grid::unit(0, "npc"),
    hjust = 0,
    # margin = unit(c(0.1, 0.1, 0.1, 0.1), "pt"),
    gp = annotation_font
  )
  
  # extract legend
  legend <- ggpubr::as_ggplot(ggpubr::get_legend(facetplot))
  
  # put everything together with patchwork
  plot <- (
    chmap +
      inset_element(facetplot + theme(legend.position = "none"),
                    left = 0, bottom = 0.1, right = 0.85, top = 0.95) +
      inset_element(legend, left = 0.05, bottom = 0.85, right = 0.2, top = 1) +
      inset_element(as.raster(logo), left = 0.85, bottom = 0, right = 1, top = 0.2)  # add pigs logo
  ) / plot_spacer() +
    plot_layout(heights = c(7, 1)) +
    inset_element(date_stamp_grob, left = .07, bottom = 0.3, right = 0.4, top = 1,
                  clip = FALSE) +
    inset_element(caption_grob, left = 0.6, bottom = 0, right = 1 , top = 1.05,
                  clip = FALSE)
  
  filename <- here("figures",
                   paste0(gsub("-", "", Sys.Date()),
                          "_chmap_", lang, ".png")
  )
  
  ragg::agg_png(filename = filename, width = 3508, height = 2480, res = 300)
  print(plot)
  dev.off()
  
  return(filename)
}

## combine geofacet age plot with swiss map ####
plot_combined_maps_age <- function(facetplot, chmap, lang, map_start_d, end_date) {
  
  # define caption in correct language
  date_stamp_text <- dplyr::case_when(
    lang == "de" ~ glue::glue(
      "RSV EpiCH (P. Agyeman, J. Trück, P. Meyer Sauteur)<br>Daten von {format(map_start_d, '%d.%m.%Y')} bis {format(end_date + 6, '%d.%m.%Y')}<br>Stand {format(Sys.Date(), '%d.%m.%Y')}"
    ),
    lang == "fr" ~ glue::glue(
      "RSV EpiCH (P. Agyeman, J. Trück, P. Meyer Sauteur)<br>Données de {format(map_start_d, '%d.%m.%Y')} à {format(end_date + 6, '%d.%m.%Y')}<br>État {format(Sys.Date(), '%d.%m.%Y')}"
    ),
    lang == "eng" ~ glue::glue(
      "RSV EpiCH (P. Agyeman, J. Trück, P. Meyer Sauteur)<br>Data from {format(map_start_d, '%d.%m.%Y')} to {format(end_date + 6, '%d.%m.%Y')}<br>Status {format(Sys.Date(), '%d.%m.%Y')}"
    )
  )
  
  annotation_font <- grid::gpar(fontsize = 11)
  
  # generate text grob for date stamp
  date_stamp_grob <- gridtext::richtext_grob(
    date_stamp_text,
    x = unit(0, "npc"),
    hjust = 0,
    gp = annotation_font
  )
  
  # extract legend
  legend <- ggpubr::as_ggplot(ggpubr::get_legend(facetplot))
  
  # put everything together with patchwork
  plot <- (
    chmap +
      inset_element(facetplot + theme(legend.position = "none"),
                    left = 0, bottom = 0.1, right = 0.85, top = 0.95) +
      inset_element(legend, left = 0.05, bottom = 0.85, right = 0.2, top = 1)
  ) / plot_spacer() +
    plot_layout(heights = c(7, 1)) +
    inset_element(date_stamp_grob, left = .07, bottom = 0.3, right = 0.4, top = 1,
                  clip = FALSE)
  
  filename <- here("figures",
                   paste0(gsub("-", "", Sys.Date()),
                          "_chmap_age_", lang, ".png")
  )
  
  ragg::agg_png(filename = filename, width = 3508, height = 2480, res = 300)
  print(plot)
  dev.off()
  
  return(filename)
}

## plot to show previous seasons against  current ####
plot_yearly <- function(rsv.dt, lang, last_date, rsv.col, plot_theme) {
  
  # pigs logo
  logo <- magick::image_read(here::here("output", "logo_pigs.png"))
  
  y.axis.text <- dplyr::case_when(
    lang == "de" ~ "Anzahl RSV Nachweise",
    lang == "fr" ~ "Nombre de détections VRS",
    lang == "eng" ~ "Number of RSV detections"
  )
  
  x.axis.text <- dplyr::case_when(
    lang == "de" ~ "Woche",
    lang == "fr" ~ "Semaine",
    lang == "eng" ~ "Week"
  )
  
  caption.text <- dplyr::case_when(
    lang == "de" ~ glue::glue("Wöchentliche Anzahl RSV Nachweise in Spitälern mit regelmässiger (>= 90%) Dateneingabe\nDie Anzahl der Spitäler welche repräsentiert sind kann sich ändern\nUnvollständige Daten werden mittels einer gestrichelten Linie dargestellt\nRSV EpiCH - Datenbestand {format(Sys.Date(), '%d.%m.%Y')}"),
    lang == "fr" ~ glue::glue("Nombre hebdomadaire de détections VRS dans les hôpitaux où les données sont régulièrement saisies (>= 90%)\nLe nombre d'hôpitaux représentés peut varier\nLes données incomplètes sont représentées par une ligne en pointillés\nRSV EpiCH - état des données au {format(Sys.Date(), '%d.%m.%Y')}"),
    lang == "eng" ~ glue::glue("Weekly number of RSV detections in hospitals with regular (>= 90%) data entry\nThe number of hospitals represented may change\nIncomplete data are shown with a dashed line\nRSV EpiCH - data status {format(Sys.Date(), '%d.%m.%Y')}")
  ) 
  
  # define completeness
  n.compl <- rsv.dt[date <= last_date, .N, by = record_id][, max(N)]
  ids <- rsv.dt[date <= last_date & !is.na(rsv), .N / n.compl, by = record_id][
    V1 > 0.9, record_id
  ]
  
  # define dates until we have good quality data
  # define current week
  w.cur <- data.table::isoweek(last_date)
  # define last complete week by site for current year
  w.last.compl <- rsv.dt[record_id %in% ids & date <= last_date & !is.na(rsv) &
           year == year(last_date),  
         .(w.max = max(week)),
         by = record_id]
  # if we are at the beginning of the year, also define last complete week for last year
  w.last.compl.prev <- rsv.dt[
    record_id %in% ids & date <= last_date &
      !is.na(rsv) & year == year(last_date) - 1L,
    .(w.max = max(week)),
    by = record_id]
  # what is the minimal week we have data in (this is the reference point until which the graph will be full scale)
  w.min <- min(w.last.compl$w.max)
  w.min.prev <- min(w.last.compl.prev$w.max)
  
  # generate overall data
  rsv.yearly <- rsv.dt[record_id %in% ids & date <= last_date,
                     .(rsv.tot = sum(rsv, na.rm = TRUE)),
                     by = .(year, week)]
  # make new plotting date
  max.y <- max(as.numeric(rsv.yearly$year))
  rsv.yearly[, plot.date := ISOweek::ISOweek2date(
    paste0(max.y, "-W", sprintf("%02d", week), "-3")
    )
  ]
  
  plot <- ggplot() +
    geom_line(data = rsv.yearly[year < (max.y - 1L), ],
              aes(week, rsv.tot, colour = year), linewidth = 1, alpha = 0.4) +
    geom_line(data = rsv.yearly[year == max.y - 1L & week <= w.min.prev, ],
              aes(week, rsv.tot, colour = year), linewidth = 1) +
    geom_line(data = rsv.yearly[year == max.y - 1L & week >= w.min.prev, ],
              aes(week, rsv.tot, colour = year), linewidth = 1, linetype = 2)
  
  # sequentially add current year data, depending on completeness
  # situation where we only have only 1 week data in the current year
  if (rsv.yearly[year == max.y, .N] == 1) {
    plot <- plot +
      geom_point(data = rsv.yearly[year == max.y & week == w.cur, ],
                 aes(week, rsv.tot, colour = year), size = 2.5)
  } else if (w.min == 1) {
    plot <- plot +
      geom_line(data = rsv.yearly[year == max.y & week >= w.min, ],
                aes(week, rsv.tot, colour = year), linewidth = 1.5, linetype = 2) +
      geom_point(data = rsv.yearly[year == max.y & week == w.cur, ],
                 aes(week, rsv.tot, colour = year), size = 1.5) 
  } else {
    plot <- plot +
      geom_line(data = rsv.yearly[year == max.y & week <= w.min, ],
                aes(week, rsv.tot, colour = year), linewidth = 1.5) +
      geom_line(data = rsv.yearly[year == max.y & week >= w.min, ],
                aes(week, rsv.tot, colour = year), linewidth = 1.5, linetype = 2) +
      geom_point(data = rsv.yearly[year == max.y & week == w.cur, ],
                 aes(week, rsv.tot, colour = year), size = 1.5) 
  }
  
  # finish plot
  plot <- plot +
    annotation_custom(grid::rasterGrob(logo, interpolate = TRUE),
                      xmin = 46, xmax = 52, ymin = -5, ymax = 30) +
    scale_colour_manual("", breaks = unique(rsv.yearly$year),
                        values = unname(rsv.col[c(2, 8, 3, 5, 7)])) +
    scale_y_continuous(y.axis.text, breaks = seq(0, 400, 50)) +
    guides(
      colour = guide_legend(
        override.aes = list(
          alpha = c(
            rep(
              0.3,
              length(unique(rsv.dt$year)) - 1L  # number of years in the data - 1
            ),
            1
          )
        )
      )
    ) +
    scale_x_continuous(x.axis.text, breaks = seq(2, 52, 2)) +
    labs(x = "", caption = caption.text) +
    coord_cartesian(clip = "off") +
    plot_theme
    
  # save plot
  filename <- here::here("figures",
                   paste0(gsub("-", "", Sys.Date()),
                          "_seasonplot_", lang, ".png")
  )
  
  ragg::agg_png(filename = filename, width = 3508, height = 2480, res = 300)
    print(plot)
  dev.off()
  
  return(filename)
}

## plot to show Inselspital data for average and current season ####
plot_insel_season <- function(dt, last_date, complete_week, powerpoint) {
  load(here("data", "historic_rsv_inselspital.RData"))
  dt <- dt[record_id == "Bern", .(record_id, week, rsv, year)]
  dt[, year := as.integer(year)]
  # add data for the missing information of season 2020/2021
  # we'll assume a 52 week year for simplicity. for 2026 will have to
  # to consider splitting data from week 53 and add to week 52 and 1
  first.part.2020 <- data.table(
    record_id = rep("Bern", 26),
    week = 27:52,
    rsv = rep(0, 26),
    year = rep(2020, 26)
  )
  # combine the data
  dt <- rbindlist(
    list(
      first.part.2020,
      dt
    )
  )
  # add date for ordering
  dt[, date := ISOweek2date(paste0(year, "-W", sprintf("%02d", week), "-1"))]
  # define season
  dt[week < 27, season := paste0(year - 1L, "/", year)]
  dt[week >= 27, season := paste0(year, "/", year + 1L)]
  dt$season <- as.factor(dt$season)
  # change to seasonal output, i.e. new year in the middle of the plot
  dt[
    week <= 26, plot.w := week + 26L
  ][
    week > 26, plot.w := week - 26L
  ]
  rsv.hist.avg[
    adm.isow <= 26, plot.w := adm.isow + 26L
  ][
    adm.isow > 26, plot.w := adm.isow - 26L
  ]
  
  # define current week for index
  index.w <- dt[date == max(dt$date), week]
  index.pw <- dt[date == max(dt$date), plot.w]
  # define current season
  cur.season <- dt[date == max(dt$date), season]
  # define breaks depending on whether current week is even or odd
  # need to look at how to deal with 53 week year
  x.breaks <- if(index.w %% 2 == 0) seq(2, 52, 2) else seq(1, 51, 2)
  x.labels <- if(index.w %% 2 == 0) c(seq(28, 52, 2), seq(2, 26, 2)) else c(seq(27, 51, 2), seq(1, 25, 2))
  
  # define max height of plot
  y.max <- c(
    max(dt$rsv, na.rm = TRUE),
    max(rsv.hist.avg$ma3.max)
  )[which.max(
    c(
      max(dt$rsv, na.rm = TRUE),
      max(rsv.hist.avg$ma3.max)
    )
  )]
  
  # load logo
  logo <- magick::image_read(here("logo_kikli_rgb_2022.png"))
  
  # make colour gradient for previous years
  ## how many seasons do we have in the dataset, subtract 1
  ny <- length(unique(dt$season)) - 1L  
  ## select colors for the previous years
  prev.insel.col <- scales::seq_gradient_pal(
    "#D1E2BC", "#6CA5DA"  # start at insel hellgruen and go to insel blue
  )(seq(0, 1, length.out = ny))
  ## add color for current season (always insel light red)
  seq.insel.col <- c(prev.insel.col, "#EE8F7A")
  
  # define alpha for previous seasons
  ## the current season and the previous season before that should have alpha = 1,
  ## the other seasons we will assign alpha = 0.8. To do this we need to find out
  ## how many data points we have in the seasons before the last 2 seasons
  n.red.alpha <- dt[season %in% levels(dt$season)[-(ny:(ny + 1L))], .N]
  ## how many data points in the last season before the current season
  prev.y.alpha <- dt[season == levels(dt$season)[ny], .N]
  red.alpha.vec <- c(rep(0.8, n.red.alpha), rep(1, prev.y.alpha))
  
  # if we show incomplete data we'll change the line type of the current season
  linetype <- if (complete_week) {1} else {8}
  
  ### construct plot ####
  plot <- ggplot() +
    # typical small season
    geom_line(aes(x = plot.w, y = ma3.max),
              data = rsv.hist.avg[season == "min"], 
              colour = c("grey60"), linewidth = 1) + #, alpha = 0.5) +  
    annotate(geom = "text", x = 45, y = 21,
             label = "typische\nkleine RSV-Saison\n(ca. 150 Patienten)",
             colour = "grey60", size = 3) + #, alpha = 0.5) +
    annotate(geom = "segment", x = 45, xend = 39, y = 19, yend = 15,
             lineend = "round", arrow = arrow(length = unit(0.1, "inches")),
             colour = "grey60", linewidth = 1) + #, alpha = 0.5) +
    # typical large season
    geom_line(aes(x = plot.w, y = ma3.max),
              data = rsv.hist.avg[season == "maj"], 
              colour = c("grey25"), linewidth = 1) + # , alpha = 0.5) + 
    annotate(geom = "text", x = 38, y = 33,
             label = "typische\ngrosse RSV-Saison\n(ca. 250 Patienten)",
             colour = "grey25", size = 3) + #, alpha = 0.5) +  
    annotate(geom = "segment", x = 38, xend = 33, y = 31, yend = 27,
             lineend = "round", arrow = arrow(length = unit(0.1, "inches")),
             colour = "grey25", linewidth = 1) + #, alpha = 0.5) + 
    # post-pandemic seasons
    geom_line(aes(x = plot.w, y = rsv, colour = as.factor(season)),
              data = dt[season != cur.season, ], linewidth = 1,
              alpha = red.alpha.vec) + #, alpha = 0.6) + 
    # current season
    geom_point(aes(x = plot.w, y = rsv, colour = as.factor(season)),
               data = dt[season == cur.season, ], size = 1.8) +
    geom_line(aes(x = plot.w, y = rsv, colour = as.factor(season)),
              data = dt[season == cur.season & date <= last_date, ],
              linewidth = 1.8) +
    # well draw the last line segment separately, thus we can have a different
    # linetype if we show incomplete data
    geom_line(aes(x = plot.w, y = rsv, colour = as.factor(season)),
              data = dt[season == cur.season & date >= last_date, ],
              linewidth = 1.8, linetype = linetype) +
    # x-axis scale
    scale_x_continuous("Kalenderwoche", breaks = x.breaks,
                       labels = x.labels) +
    # add index for current week
    annotate(geom = "point", x = index.pw, y = -2.99, colour = "red",
             size = 6.8, shape = 1) +
    # add start of Nirsevimab availability
    annotate(geom = "segment", x = 16, xend = 16, y = -0.5, yend = -1.5,
             colour = "red", linewidth = 1) +
    annotate(geom = "segment", x = 16, xend = 18, y = -1, yend = -1,
             colour = "red", linewidth = 1) +
    annotate(geom = "text", x = 21, y = -1, label = "Nirsevimab",
             size = 4.5, colour = "red") +
    annotate(geom = "segment", x = 24, xend = 27, y = -1, yend = -1,
             colour = "red", linewidth = 1,
             arrow = grid::arrow(length = unit(0.1, "inches"), type = "closed")) +
    # add logo
    annotation_custom(grid::rasterGrob(logo, interpolate = TRUE),
                      xmin = 40, xmax = 52, ymin = y.max -1, ymax = y.max + 9) +
    # make plot nice
    scale_color_manual(values = seq.insel.col) +
    guides(
      colour = guide_legend("") #, override.aes = list(alpha = c(0.3, 0.3, 1)))
    ) +
    labs(
      title = "Verlauf RSV-Saison MB KIJU Inselspital Bern",
      y = "Anzahl RSV Nachweise"
    ) +
    coord_cartesian(ylim = c(0, y.max + 1L), clip = "off") +
    theme(
      panel.background = element_rect(fill = "white"),
      panel.grid.major.x = element_blank(),
      panel.grid.major.y = element_line(colour = "grey95", linetype = 2),
      panel.grid.minor.x = element_blank(),
      panel.grid.minor.y = element_blank(),
      axis.line.x = element_line(linewidth = 0.5),
      axis.line.y = element_line(linewidth = 0.5),
      legend.key = element_rect(fill = NA),
      legend.background = element_rect(fill = NA),
      legend.position.inside = c(0.15, 0.82),
      text = element_text(size = 12)
    )
  
  ### plot for powerpoint full-size ####
  ppt.lw <- 0.8
  ppt.lw.emph <- 1.2
  plot.ppt <- ggplot() +
    # typical small season
    geom_line(aes(x = plot.w, y = ma3.max),
              data = rsv.hist.avg[season == "min"], 
              colour = c("grey60"), linewidth = ppt.lw) + #, alpha = 0.5) +  
    annotate(geom = "text", x = 45, y = 24,
             label = "Vor 2021\n'kleine' Saison\n(ca. 150 Patienten)",
             colour = "grey60", size = 2.8) + #, alpha = 0.5) +
    annotate(geom = "segment", x = 45, xend = 39, y = 19, yend = 15,
             lineend = "round", arrow = arrow(length = unit(0.07, "inches"), type = "closed"),
             colour = "grey60", linewidth = ppt.lw) + #, alpha = 0.5) +
    # typical large season
    geom_line(aes(x = plot.w, y = ma3.max),
              data = rsv.hist.avg[season == "maj"], 
              colour = c("grey25"), linewidth = ppt.lw) + # , alpha = 0.5) + 
    annotate(geom = "text", x = 38, y = 36,
             label = "Vor 2021\n'grosse' Saison\n(ca. 250 Patienten)",
             colour = "grey25", size = 2.8) + #, alpha = 0.5) +  
    annotate(geom = "segment", x = 38, xend = 33, y = 31, yend = 27,
             lineend = "round", arrow = arrow(length = unit(0.07, "inches"), type = "closed"),
             colour = "grey25", linewidth = ppt.lw) + #, alpha = 0.5) + 
    # post-pandemic seasons
    geom_line(aes(x = plot.w, y = rsv, colour = as.factor(season)),
              data = dt[season != cur.season, ], linewidth = ppt.lw,
              alpha = red.alpha.vec) + #, alpha = 0.6) + 
    # current season
    geom_point(aes(x = plot.w, y = rsv, colour = as.factor(season)),
               data = dt[season == cur.season, ], size = ppt.lw.emph) +
    geom_line(aes(x = plot.w, y = rsv, colour = as.factor(season)),
              data = dt[season == cur.season & date <= last_date, ],
              linewidth = ppt.lw.emph) +
    # well draw the last line segment separately, thus we can have a different
    # linetype if we show incomplete data
    geom_line(aes(x = plot.w, y = rsv, colour = as.factor(season)),
              data = dt[season == cur.season & date >= last_date, ],
              linewidth = ppt.lw.emph, linetype = linetype) +
    # x-axis scale
    scale_x_continuous("Kalenderwoche", breaks = x.breaks,
                       labels = x.labels) +
    # add index for current week
    annotate(geom = "point", x = index.pw, y = -4.1, colour = "red",
             size = 6.8, shape = 1) +
    # add start of Nirsevimab availability
    annotate(geom = "segment", x = 16, xend = 16, y = -0.5, yend = -1.9,
             colour = "red", linewidth = 0.5) +
    annotate(geom = "segment", x = 16, xend = 18, y = -1.2, yend = -1.2,
             colour = "red", linewidth = 0.5) +
    annotate(geom = "text", x = 21, y = -1.05, label = "Nirsevimab",
             size = 3.5, colour = "red") +
    annotate(geom = "segment", x = 24, xend = 27, y = -1.2, yend = -1.2,
             colour = "red", linewidth = 0.5,
             arrow = grid::arrow(length = unit(0.07, "inches"), type = "closed")) +
    # add logo
    # annotation_custom(grid::rasterGrob(logo, interpolate = TRUE),
    #                   xmin = 40, xmax = 52, ymin = y.max -1, ymax = y.max + 9) +
    # make plot nice
    scale_color_manual(values = seq.insel.col) +
    guides(
      colour = guide_legend("") #, override.aes = list(alpha = c(0.3, 0.3, 1)))
    ) +
    labs(
      title = "Verlauf RSV-Saison MB KIJU Inselspital Bern",
      y = "Anzahl RSV Nachweise"
    ) +
    coord_cartesian(ylim = c(0, y.max + 1L), clip = "off") +
    theme(
      panel.background = element_rect(fill = "white"),
      panel.grid.major.x = element_blank(),
      panel.grid.major.y = element_line(colour = "grey95", linetype = 2),
      panel.grid.minor.x = element_blank(),
      panel.grid.minor.y = element_blank(),
      axis.line.x = element_line(linewidth = 0.5),
      axis.line.y = element_line(linewidth = 0.5),
      legend.key = element_rect(fill = NA),
      legend.background = element_rect(fill = NA),
      legend.position.inside = c(0.15, 0.82),
      text = element_text(size = 10)
    )
  
  ### save the plot ####
  filename <- here("figures",
                   paste0(gsub("-", "", Sys.Date()),
                          "_insel_season_de.png")
  )
  
  if (powerpoint) {
    ragg::agg_png(
      filename = filename, width = 22.42, height = 8.8, units = "cm", res = 300
    )
    print(plot.ppt)
    dev.off()
  } else {
    ragg::agg_png(
      filename = filename, width = 3508, height = 2480, res = 300
    )
    print(plot)
    dev.off()
  }
  
  return(filename)
  
  # ggsave(filename = here("figures", "rsv.kiju.season.png"),
  #        width = 11, height = 7)
  # ggsave(filename = here("output", "rsv.kiju.season.pdf"),
  #        width = 11, height = 7)
  # ggsave(filename = here("output", "rsv.kiju.season.eps"),
  #        width = 11, height = 7, units = "in")
}
