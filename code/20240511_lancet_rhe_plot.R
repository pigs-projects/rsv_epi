library(here)
library(data.table)
library(ggplot2)
library(ggtext)
library(patchwork)

load(here("data", "20240610_lancet.RData"))

plot.start.date <- as.Date("2020-12-15")
plot.end.date <- as.Date("2024-05-27")

# plot missing data ####
# rsv[, rsv.max := max(rsv, na.rm = TRUE), by = .(record_id)] |> 
#   _[, rsv.norm := rsv / rsv.max] |> 
#   ggplot() +
#   ggridges::geom_ridgeline(
#     aes(x = date, y = record_id,
#         height = rsv.norm, group = record_id)
#   )


# figure 1 - Lancet RHE ####
## figure 1 - top panel ####
errbar_y <- 590
errbar_text_y <- 620
errbar_w <- 30
errbar_text_size <- 3.4

fig1_data <- melt(
  rsv,
  id.vars = c("date", "record_id"),
  measure.vars = c("rsv_less_1y", "rsv_1to2y", "rsv_over_2y"),
  variable.name = "age.grp",
  value.name = "rsv",
)[, .(tot.rsv = sum(rsv, na.rm = TRUE)), by = .(date, age.grp)]

fig1 <- ggplot(fig1_data, aes(date, tot.rsv, fill = forcats::fct_rev(age.grp))) +
  geom_col() +
  scale_fill_brewer(
    "", type = "qual",
    labels = c("2-16 years", "12 to 23 months", "0-11 months")) +
  scale_y_continuous("Number of RSV detections", breaks = seq(0, 600, 200)) +
  scale_x_date(
    "",
    breaks = as.Date(
      c("2021-01-01", "2021-07-01", "2022-01-01", "2022-07-01", "2023-01-01",
        "2023-07-01", "2024-01-01")
    ),
    date_labels = "%B\n%Y") +
  # add annotations for the 3 seasons we defined
  ## 2021/2022 pandemic period
  annotate(
    geom = "errorbar", xmin = as.Date("2021-01-04"),
    xmax = as.Date("2022-06-30"), y = errbar_y, width = errbar_w
  ) +
  annotate(
    geom = "text", x = as.Date("2021-10-01"), y = errbar_text_y,
    label = "2021/2022 pandemic period", size = errbar_text_size
  ) +
  ## 2022/2023 winter season
  annotate(
    geom = "errorbar", xmin = as.Date("2022-07-04"),
    xmax = as.Date("2023-06-30"), y = errbar_y, width = errbar_w
  ) +
  annotate(
    geom = "text", x = as.Date("2023-01-01"), y = errbar_text_y,
    label = "2022/2023 winter season", size = errbar_text_size
  ) +
  ## 2023/2024 winter season
  annotate(
    geom = "errorbar", xmin = as.Date("2023-07-03"),
    xmax = max(rsv$date) + 7, y = errbar_y, width = errbar_w
  ) +
  annotate(
    geom = "errorbar", xmin = max(rsv$date),
    xmax = max(rsv$date) + 7, y = errbar_y, width = errbar_w + 0.5,
    colour = "white",
    linewidth = 0.7
  ) +
  annotate(
    geom = "text", x = as.Date("2023-11-25"), y = errbar_text_y,
    label = "2023/2024 winter season", size = errbar_text_size
  ) +
  coord_cartesian(xlim = c(plot.start.date, plot.end.date)) +
  theme(
    # panel.grid = element_blank(),
    panel.grid.major.x = element_line(colour = "grey95"),
    panel.grid.minor.y = element_blank(),
    panel.background = element_blank(),
    axis.line = element_line(linewidth = 0.5),
    text = element_text(size = 15, family = "Times New Roman"),
    legend.text = element_text(size = 11),
    strip.background = element_rect(fill = "white", colour = NA),
    legend.position = "inside",
    legend.position.inside = c(0.2, 0.67)
  )

# fig1
# ggsave(
#   here("output", "20240123_fig1.png"),
#   plot = fig1,
#   width = 11.693,
#   height = 8.268
# )

## figure 1 - bottom panel ####
sentinella.labels <- c(
  region_1 = paste0("<img src='", here('output', 'sentinella1.jpg'), "' width ='90'/>"),
  region_2 = paste0("<img src='", here('output', 'sentinella2.jpg'), "' width ='90'/>"),
  region_3 = paste0("<img src='", here('output', 'sentinella3.jpg'), "' width ='90'/>"),
  region_4 = paste0("<img src='", here('output', 'sentinella4.jpg'), "' width ='90'/>"),
  region_5 = paste0("<img src='", here('output', 'sentinella5.jpg'), "' width ='90'/>"),
  region_6 = paste0("<img src='", here('output', 'sentinella6.jpg'), "' width ='90'/>")
)

errbar_y <- 7.2
errbar_text_y <- 7.3
errbar_w <- 0.1

rsv.rescale <- function(data, group) {
  data <- data[
    sentinella == group,
    .(rsv = sum(rsv_less_1y, rsv_1to2y, rsv_over_2y, na.rm = TRUE)),
    by = .(date)
  ]
  max.rsv <- max(data$rsv, na.rm = TRUE)
  data[, .(rsv.scaled = scales::rescale(rsv, from = c(0, max.rsv)),
           sentinella = group),
       by = .(date)]
}

fig2_data <- rbindlist(
  list(
    rsv.rescale(rsv, "region_1"),
    rsv.rescale(rsv, "region_2"),
    rsv.rescale(rsv, "region_3"),
    rsv.rescale(rsv, "region_4"),
    rsv.rescale(rsv, "region_5"),
    rsv.rescale(rsv, "region_6")
  )
)

fig2 <- ggplot(fig2_data) +
  ggridges::geom_ridgeline(
    aes(x = date, y = forcats::fct_rev(sentinella),
        height = rsv.scaled, group = sentinella),
    scale = 0.95
  ) +
  scale_x_date(
    "",
    breaks = as.Date(
      c("2021-01-01", "2021-07-01", "2022-01-01", "2022-07-01", "2023-01-01",
        "2023-07-01", "2024-01-01")
    ),
    date_labels = "%B\n%Y"
  ) +
  scale_y_discrete(
    "Sentinella region",
    # labels = c("6 (GR, TI)", "5 (SG, SH, ZH)", "4 (LU)", "3 (AG, BS)",
    #            "2 (BE, FR)", "1 (GE, NE, VD, VS)")
    labels = sentinella.labels
  ) +
  # add annotations for the 3 seasons we defined
  ## 2021/2022 pandemic period
  annotate(
    geom = "errorbar", xmin = as.Date("2021-01-04"),
    xmax = as.Date("2022-06-30"), y = errbar_y, width = errbar_w
  ) +
  annotate(
    geom = "text", x = as.Date("2021-10-01"), y = errbar_text_y,
    label = "2021/2022 pandemic period", size = errbar_text_size
  ) +
  ## 2022/2023 winter season
  annotate(
    geom = "errorbar", xmin = as.Date("2022-07-04"),
    xmax = as.Date("2023-06-30"), y = errbar_y, width = errbar_w
  ) +
  annotate(
    geom = "text", x = as.Date("2023-01-01"), y = errbar_text_y,
    label = "2022/2023 winter season", size = errbar_text_size
  ) +
  ## 2023/2024 winter season
  annotate(
    geom = "errorbar", xmin = as.Date("2023-07-03"),
    xmax = max(rsv$date) + 7, y = errbar_y, width = errbar_w
  ) +
  annotate(
    geom = "errorbar", xmin = max(rsv$date),
    xmax = max(rsv$date) + 7, y = errbar_y, width = errbar_w + 0.5,
    colour = "white",
    linewidth = 0.7
  ) +
  annotate(
    geom = "text", x = as.Date("2023-11-25"), y = errbar_text_y,
    label = "2023/2024 winter season", size = errbar_text_size
  ) +
  # theme_ridges()
  theme(
    # panel.grid = element_blank(),
    panel.grid.major.x = element_line(colour = "grey95"),
    panel.grid.major.y = element_line(colour = "grey95"),
    panel.background = element_blank(),
    axis.line = element_line(linewidth = 0.5),
    text = element_text(size = 15, family = "Times New Roman"),
    axis.text.y = element_markdown(size = 14, family = "Times New Roman"),
    strip.background = element_rect(fill = "white", colour = NA)
  ) +
  coord_cartesian(xlim = c(plot.start.date, plot.end.date),
                  ylim = c(1, 7.5))

## save plot ####

lancet_fig <- fig1 / fig2 +
  patchwork::plot_layout(ncol = 1, heights = c(1, 3)) +
  patchwork::plot_annotation(tag_levels = "a")

ggsave(here("output", "20240511_fig1_lancet.tif"), plot = lancet_fig,
       width = 9.268, height = 13.3, dpi = 600,
       device = "tiff")
